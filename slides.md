% CICD - few words
% Szymon Rogalski
% 2022-05-09

# Short course

[short video](https://youtu.be/jZYrxk2WMbY)


# CICD - google results:

What Google show: [Google result](https://www.google.com/search?q=CICD&rlz=1C1GCEB_plPL988PL988&sxsrf=ALiCzsarrwfef7SGoQ3gZkchv7nakF5ZrA:1652114570690&source=lnms&tbm=isch&sa=X&ved=2ahUKEwiFz_Sw7tL3AhXrh4sKHXNiBy0Q_AUoAXoECAEQAw&biw=1536&bih=722&dpr=1.25).


# CICD - GitLab

![](https://docs.gitlab.com/ee/ci/introduction/img/gitlab_workflow_example_11_9.png)

more [here](https://docs.gitlab.com/ee/ci/introduction/)


# About this project

* we have two stages (build, deploy)
* during build we create two artifacts (html, pdf)
* during commit to main branch we deploy artefacts, on our website:

[slides html](https://pirgos.gitlab.io/2_cicd_slides/)
[slides pdf](https://pirgos.gitlab.io/2_cicd_slides/slides.pdf)


# TODO 

* create branch, and add someting to our presentation
* if build is ok, create merge request to main branch

